# PokeSave Planner
A React/Redux application that uses the https://www.pokeapi.co/ API to 
help plan main line game playthroughs.

---


## Deployment Link
You can find a deployed version of the site on 
[GitLab Pages](https://carpinelli.gitlab.io/web/awbps/projects/pokesav-planner/).

---


## Intended Features
### Evolution Planner
Plan your evolution timing around the moves available post evolution-delay.
### Pokemon Information Center
Get information on a specific pokemon using the current URL. Will use
useEffect to get information on page load. Should also check for stored
data before requesting data. If the data must be requested, store it.
### Ensure working on mobile
Test layout is working on mobile.


## Unresolved Issues
* Direct links to pages (other than home) cause a 404 in production.
* There is still whitespace/unstyled HTML at the bottom of a page that
shows when showing the content of a dropdown/details element.

---


## Technologies
* React
* ReduxJS
* React-Router-DOM
* React-Markdown
* Axios
* TailwindCSS
* PokeAPI.co/
* GitLab Pages
* * Docker


## Approach
Design a typical looking website based on the popular franchise's
themes that uses all technologies taught in the PerScholas/ActivateWork
bootcamp. This tool should and will be useful to myself in planning
personal playthroughs. This site should be equally functional on a
mobile device.

---


## Project Requirements
* [x] Built with HTML, CSS, JavaScript using the React Library
* [x] Hosted on CodeSandbox or Netlify (GitLab Pages)
* [x] Frequent Commits to github (GitLab)
* [x] A README.md ﬁle with explanations of the technologies
used, the approach taken, a link to your live site,
installation instructions, unsolved problems, etc.
* [x] Use Fetch and UseEﬀect to make a request to an
external data source like OMDBapi and insert some of
the data retrieved into your State and display it on the
screen
### Bonus Requirements
* [ ] Have one or more complex user interface modules such
as a carousel, drag and drop, a sticky nav, tooltips, etc
* [ ] Look into localstorage so you can save data to the user's
browser
* [x] Utilize Redux or the Context API
### Previous React Lessons Not Required
* [x] CSS Framework (TailwindCSS)
* [x] React-Router-DOM: BrowserRouter, Routes, Links, and useNavigate
* [x] useRef

---


## Building
### React/Vite
    cd <PROJECTS/BUILDS_DIRECTORY>
    git clone <GIT_URL> <DIR_NAME>/
    cd <DIR_NAME>/
    npm install
    npm run build


## Contact
Joseph Carpinelli <carpinelli.dev@protonmail.ch>.

---


## Contributing
This is mostly a personal project, but any contributions are welcome and
appreciated, provided they align with the licenses, styles, and goals of
the project. Please see the [CONTRIBUTING.md](CONTRIBUTING.md) file, if
available. Forking or copying is encouraged, just ensure to uphold the
licenses terms.

---


## License
The software and related files in this project are licensed under the
AGPL 3.0 license or higher. All non-software related files are licensed
under the Creative Commons Attribution Share Alike 4.0 International
license. Unless any libraries used legally require otherwise, all
contributions are made under the
[GNU Affero General Public License v3 or higher](https://www.gnu.org/licenses/agpl-3.0.html).
See the [LICENSE](LICENSE).

In cases where a library is used that requires a different or additional
license, and that license is not included, kindly inform the email
provided in the "Contact" section. Corrections will be made ASAP.

You can be released from the requirements of the above license by
purchasing a commercial license. Buying such a license is mandatory
if you want to modify or otherwise use the software for commercial
activities involving the software without disclosing the source code
of your own applications. Please note this may not always be possible
due to  the licensing requirements of included projects.

To purchase a commercial license, where available, send an
email to <carpinelli.dev@protonmail.ch>.

---

# Style Guidelines

## Code Blocks
All code blocks should have the opening character on a newline.

<hr>

## JavaScript/TypeScript

### Functions
Function statements only.

### Statements
All statements should end with a semicolon, where possible.

<hr>

## More Style Guidelines
These are not the only style guidelines. More will be added,
and are in active effect.

### To Add:
* Directory structure
* Filenames
* Variable names
* Line spacing
* More/etc.

<hr>

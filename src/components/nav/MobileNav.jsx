import { useRef } from "react";
import { Link } from "react-router-dom"


const MobileNav = function()
{
    const mobileMenuRef = useRef(<></>);

    const handleClick = function(_)
    {
        console.log(mobileMenuRef.current);
        mobileMenuRef.current.classList.toggle("hidden");
    };

    
    return (
        <div onClick={handleClick} className="nav-mobile">
            <div className="hidden mobile-menu" ref={mobileMenuRef}>
                <Link to="/pokeinfo-center">
                    <h4 className="nav-component">PokeInfo Center</h4>
                </Link>
                <Link to="/evolution-planner">
                    <h4 className="nav-component">Evolution Planner</h4>
                </Link>
                <Link to="/about">
                    <h4>About</h4>
                </Link>
            </div>
            {/* Get Hamburger Menu Icon */}
            <svg
                xmlns="https://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="hamburger-icon">
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M4 6h16M4 12h16M4 18h16" />
            </svg>
        </div>
    );
};


export default MobileNav;

import { Link } from "react-router-dom";


const FullNav = function()
{
    return (
        <div className="nav-full">
            <div className="site-sections">
                <Link to="/pokeinfo-center">
                    <h4 className="nav-component">PokeInfo Center</h4>
                </Link>
                <Link to="/evolution-planner">
                    <h4 className="nav-component">Evolution Planner</h4>
                </Link>
            </div>
            <div className="nav-component">
                <Link to="/about"><h4>About</h4></Link>
            </div>
        </div>
    );
};


export default FullNav;

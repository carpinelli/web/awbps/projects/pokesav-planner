import { Link } from "react-router-dom";

import FullNav from "./FullNav";
import MobileNav from "./MobileNav";


const NavBar = function()
{
    return (
        <section className="nav-bar component">
            <div className="home-logo nav-component">
                <Link to="/"><h2>PokeSav Planner</h2></Link>
            </div>
            <MobileNav />
            <FullNav />
        </section>
    );
};


export default NavBar;

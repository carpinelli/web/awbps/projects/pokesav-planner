import { useState } from "react";
import { useNavigate } from "react-router-dom";

import capitalize from "../utilities/capitalize";
import { useGetAllPokemonQuery } from "../services/pokeapi";


const PokeSelector = function()
{
    // Hooks.

    const [selectedPokemon, setSelectedPokemon] = useState("");
    const { data, error, isLoading } = useGetAllPokemonQuery();
    const navigate = useNavigate();

    // Functions.

    const pokemonSelectOptions = function(data)
    {
        const allPokemon = Array.from(data.results);
        const selectOptions = allPokemon.map((pokemon) =>
        {
            return (
                <option
                 key={pokemon.url}
                 value={pokemon.name}>
                    {capitalize(pokemon.name)}
                </option>
            );
        });
        selectOptions.unshift(<option key="0" value="">--Please choose an option--</option>);
        return selectOptions;
    };

    const buildSelectOptions = function(data, error, isLoading)
    {
        // Build dropdown.
        return (error ? <option key="-1"value="loadError">Load Error!</option>
                : isLoading ? <option key="0" value="loading">Loading...</option>
                : pokemonSelectOptions(data));
    };

    const handleSelectChange = function(event)
    { setSelectedPokemon(event.target.value); }

    const handleClick = function(event)
    {
        event.preventDefault();  // Prevent page reload.
        if (selectedPokemon)
        { navigate(`/pokeinfo-center/${selectedPokemon}`); }
        else
        { alert("Please select a proper entry form the dropdown."); }
    };



    return (
        <form className="component">
            <ul>
                <li>
                    <label htmlFor="pokemon-select">Pokemon: </label>
                    <select name="aPokemon"
                     id="pokemon-select"
                     className="bg-black"
                     value={selectedPokemon}
                     onChange={handleSelectChange}
                    >
                        {buildSelectOptions(data, error, isLoading)}
                    </select>
                    <button className="button" onClick={handleClick}>Go!</button>
                </li>
            </ul>
        </form>
    );
};


export default PokeSelector;
    
import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const FormsDisplay = function({ forms })
{
    const formsJSX = Array.from(forms).map((form) =>
    {
        return (
            <div
             key={form.url}
             className="info-row">
                <p className="info-entry mr-auto">
                    <span className="label">Form:</span>
                    {capitalCase(form.name)}
                </p>
                <Link to={`/forminfo-center/${form.name}`}>
                    <button className="button">Form Info</button>
                </Link>
            </div>
        )
    });

    return (
        <details>
            <summary>Forms</summary>
            <section>{formsJSX}</section>
        </details>
    );
};


export default FormsDisplay;

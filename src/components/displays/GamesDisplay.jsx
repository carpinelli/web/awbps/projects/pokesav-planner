import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const GamesDisplay = function({ game_indices })
{
    const gamesJSX = Array.from(game_indices).map((game) =>
    {
        return (
            <div
             key={game.version.url}
             className="info-row">
                <p className="info-entry mr-auto">
                    <span className="label">{`Game: `}</span>
                    {`${capitalCase(game.version.name)} Version`}
                </p>
                <Link to={`/gameinfo-center/${game.version.name}`}>
                    <button className="button">Game Info</button>
                </Link>
            </div>
        );
    });

    return (
        <details>
            <summary>Games</summary>
            <section>{gamesJSX}</section>
        </details>
    );
};


export default GamesDisplay;

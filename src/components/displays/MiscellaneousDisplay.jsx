const MiscellaneousDisplay = function({ pokemon })
{
    const heldItemsJsx = (!pokemon.held_items[0]
                          ? pokemon.held_items
                          : Array.from(pokemon.held_items).map((entry) =>
                          {
                            return (
                                <p key={entry.item.url} className="bg-primary p-2 my-2">
                                    <span className="label">Held Item:</span>
                                    {entry.item.name}
                                </p>
                            );
                          }));

    return (
        <details>
            <summary>Miscellaneous</summary>
            <section className="text-secondary ml-2">
                <p className="bg-primary p-2 my-2">
                    <span className="label">Base Experience: </span>
                    {pokemon.base_experience}
                </p>
                <p className="bg-primary p-2 my-2">
                    <span className="label">Height: </span>
                    {pokemon.height}
                </p>
                <div>
                    <p className="bg-primary p-2 my-2">
                        <span className="label">Held Items: </span>
                    </p>
                    {heldItemsJsx}
                </div>
                <p className="bg-primary p-2 my-2">
                    <span className="label">ID: </span>
                    {pokemon.id}
                </p>
                <p className="bg-primary p-2 my-2">
                    <span className="label">Is Default: </span>
                    {pokemon.is_default}
                </p>
                <p className="bg-primary p-2 my-2">
                    <span className="label">Location Area Encounters: </span>
                    {pokemon.location_area_encounters}
                </p>
                <p className="bg-primary p-2 my-2">
                    <span className="label">Order: </span>
                    {pokemon.order}
                </p>
                <p className="bg-primary p-2 my-2">
                    <span className="label">Past Types: </span>
                    {pokemon.past_types}
                </p>
                <p className="bg-primary p-2 my-2">
                    <span className="label">Weight: </span>
                    {pokemon.weight}
                </p>
            </section>
        </details>
    );
};


export default MiscellaneousDisplay;

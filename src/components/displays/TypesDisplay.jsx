import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const TypesDisplay = function({ types })
{
    const typesJSX = Array.from(types).map((type) =>
    {
        return (
            <div key={type.type.url} className="info-row">
                <p className="info-entry mr-auto">
                    <span className="label">
                        {`Type ${type.slot}:`}
                    </span>
                    {capitalCase(type.type.name)}
                </p>
                <Link to={`/typeinfo-center/${type.type.name}`}>
                    <button className="button">Type Info</button>
                </Link>
            </div>
        );
    });

    return (
        <details>
            <summary>Types</summary>
            <section>{typesJSX}</section>
        </details>
    );
};


export default TypesDisplay;

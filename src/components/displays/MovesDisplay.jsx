import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const MovesDisplay = function({ moves })
{
    const movesJSX = Array.from(moves).map((move) =>
    {
        return (
            <div
             key={move.move.url}
             className="info-row">
                <p className="info-entry">
                    <span className="label pr-1">Move:</span>
                    {capitalCase(move.move.name, "-")}
                </p>
                <p className="info-entry">
                    <span className="label">Level Learned At:</span>
                    {move.version_group_details[0].level_learned_at}
                </p>
                <p className="info-entry mr-auto">
                    <span className="label">Game Version:</span>
                    {capitalCase(move.version_group_details[0].version_group.name, "-")}
                </p>
                <Link to={`/moveinfo-center/${move.move.name}`}>
                    <button className="button">Move Info</button>
                </Link>
            </div>
        );
    });

    return (
        <details>
            <summary>Moves</summary>
            <section>{movesJSX}</section>
        </details>
    );
};


export default MovesDisplay;

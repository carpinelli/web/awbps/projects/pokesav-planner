import capitalCase from "../../utilities/capitalize";
import AbilitiesDisplay from "./AbilitiesDisplay";
import FormsDisplay from "./FormsDisplay";
import GamesDisplay from "./GamesDisplay";
import MiscellaneousDisplay from "./MiscellaneousDisplay.jsx"
import MovesDisplay from "./MovesDisplay";
import SpeciesDisplay from "./SpeciesDisplay";
import SpritesDisplay from "./SpritesDisplay";
import StatsDisplay from "./StatsDisplay";
import TypesDisplay from "./TypesDisplay";


const PokeDisplay = function({ pokemon, additionalClasses = "" })
{
    return (!pokemon.name
        ? <p className={["component", additionalClasses].join(" ")}>"Loading..."</p>
        : (
        <div className={["component", additionalClasses].join(" ")}>
            <h2>{capitalCase(pokemon.name)}</h2>
            <AbilitiesDisplay abilities={pokemon.abilities}/>
            <FormsDisplay forms={pokemon.forms} />
            <GamesDisplay game_indices={pokemon.game_indices} />
            <MiscellaneousDisplay pokemon={pokemon} />
            <MovesDisplay moves={pokemon.moves} />
            <SpeciesDisplay species={pokemon.species} />
            <SpritesDisplay sprites={pokemon.sprites} />
            <StatsDisplay stats={pokemon.stats} />
            <TypesDisplay types={pokemon.types} />
        </div>
        )
    );
};


export default PokeDisplay

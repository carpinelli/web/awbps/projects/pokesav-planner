import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const SpritesDisplay = function({ sprites })
{
    const spritesJSX = (
        <Link to="/about">
            <p className="info-row pl-2">{`${capitalCase("under")} maintenance...`}</p>
        </Link>
    );

    return (
        <details>
            <summary>Sprites</summary>
            <section>{spritesJSX}</section>
        </details>
    );
};


export default SpritesDisplay;

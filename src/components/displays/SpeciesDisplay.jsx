import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const SpeciesDisplay = function({ species })
{
    const speciesJSX = (
        <div
         key={species.url}
         className="info-row">
            <p className="info-entry mr-auto">
                <span className="label">Species:</span>
                {capitalCase(species.name)}
            </p>
            <Link to={`/speciesinfo-center/${species.name}`}>
                <button className="button">Species Info</button>
            </Link>
        </div>
    );

    return (
        <details>
            <summary>Species</summary>
            <section>{speciesJSX}</section>
        </details>
    );
};


export default SpeciesDisplay;

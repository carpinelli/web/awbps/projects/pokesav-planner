import { Link } from "react-router-dom";

import capitalCase from "../../utilities/capitalize";


const StatsDisplay = function({ stats })
{
    const statsJSX = Array.from(stats).map((stat) =>
    {
        return (
            <div
             key={stat.stat.url}
             className="info-row">
                <p className="info-entry">
                    <span className="label">
                        {`Stat ${capitalCase(stat.stat.name)}:`}
                    </span>
                </p>
                <p className="info-entry">
                    <span className="label">Base Stat Value:</span>
                    {stat.base_stat}
                </p>
                <p className="info-entry mr-auto">
                    <span className="label">Base Effort:</span>
                    {stat.effort}
                </p>
                <Link to={`/typeinfo-center/${stat.stat.name}`}>
                    <button className="button">Stat Info</button>
                </Link>
            </div>
        );
    });

    return (
        <details>
            <summary>Stats</summary>
            <section>{statsJSX}</section>
        </details>
    );
};


export default StatsDisplay;

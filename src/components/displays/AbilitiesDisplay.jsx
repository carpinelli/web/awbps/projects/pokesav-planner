import { Link } from "react-router-dom";
import capitalCase from "../../utilities/capitalize";

const AbilitiesDisplay = function({ abilities })
{
    const clearAbilities = Array.from(abilities).map((ability) =>
    {
        return (
            <div
             key={ability.ability.url + ability.is_hidden}
             className="info-row">
                <p className="info-entry">
                    <span className="label">{`Ability Slot ${ability.slot}:`}</span>
                </p>
                {ability.is_hidden ? <p className="pr-1">Hidden Ability</p> : <></>}
                <p className="info-entry mr-auto">
                    {`${capitalCase(ability.ability.name)}`}
                </p>
                <Link to="/abilityinfo-center">
                    <button className="button">Ability Info</button>
                </Link>
            </div>
        );
    });

    return (
        <details>
            <summary>Abilities</summary>
            <section>{clearAbilities}</section>
        </details>
    );
};


export default AbilitiesDisplay;

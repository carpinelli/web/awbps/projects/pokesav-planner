import { Route, Routes } from "react-router-dom";

import PageLayout from "./pages/PageLayout";
import Home from "./pages/Home";
import PokeInfoCenter from "./pages/PokeInfoCenter";
import PokeInfo from "./pages/PokeInfo"
import EvolutionPlanner from "./pages/EvolutionPlanner";
import About from "./pages/About";


const App = function()
{
  const home = (<PageLayout><Home /></PageLayout>);
  const pokeinfoCenter = (<PageLayout><PokeInfoCenter /></PageLayout>);
  const pokeinfo = (<PageLayout><PokeInfo /></PageLayout>);
  const evolutionPlanner = (<PageLayout><EvolutionPlanner /></PageLayout>);
  const about = (<PageLayout><About /></PageLayout>);

  return (
    <main className="app">
      <Routes>
        <Route path="/" element={home} />
        <Route path="/pokeinfo-center" element={pokeinfoCenter} />
        <Route path="/pokeinfo-center/:pokemonName" element={pokeinfo} />
        <Route path="/evolution-planner" element={evolutionPlanner} />
        <Route path="/about" element={about} />
      </Routes>
    </main>
  );
};

export default App;

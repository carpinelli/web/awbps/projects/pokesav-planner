import ReactMarkdown from "react-markdown";

import README from "../data/README";
import Header from "../components/Header";


const About = function()
{
    return (
        <section className="page">
            <Header text="About Page" />
            <ReactMarkdown className="readme">{README}</ReactMarkdown>
        </section>
    );
};



export default About;
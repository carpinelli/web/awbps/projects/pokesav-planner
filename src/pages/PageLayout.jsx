import NavBar from "../components/nav/NavBar";


const PageLayout = function({ children })
{
    return (
        <article>
            <NavBar />
            {children}
        </article>
    );
};


export default PageLayout;

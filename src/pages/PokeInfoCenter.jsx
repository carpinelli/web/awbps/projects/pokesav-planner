import Header from "../components/Header";
import PokeSelector from "../components/PokeSelector";


const PokeInfoCenter = function()
{
    return (
            <section className="page">
                <Header text="PokeInfo Center" />
                <p className="text-xl">Welcome to the PokeInfo Center!</p>
                <hr />
                <br />
                <PokeSelector />
            </section>
        );
};


export default PokeInfoCenter;

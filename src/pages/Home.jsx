import Header from "../components/Header";


const Home = function()
{
    return (
        <section className="page">
            <Header text="Home Page" />
            <section>
                <p className="text-xl">Welcome to PokeSav Planner!</p>
                <hr />
                <p>This app is meant to help you plan playthroughs of a popular franchise.</p>
                <p>It uses <a href="https://www.pokeapi.co/">PokeAPI</a> to fetch public data about the franchise and then uses it to help the player make decesions in their playthrough.</p>
                <br /><br />
            </section>
            <section>
                <p className="text-xl">Current Features</p>
                <hr /><br />
                <p className="text-lg">PokeInfo Center</p>
                <p>Here you can get all the public info available for each monster.</p>
                <br />
            </section>
            <section>
                <p className="text-lg">Evolution Planner</p>
                <p>Currently under maintenance. Once complete, you will be able to enter information about your monster to help decide if delaying an evolution to get a move sooner is worth it.</p>
                <br />
            </section>
        </section>
    );
};



export default Home;

import { useEffect, useState } from "react";
import { useParams } from "react-router";

import { useGetPokemonByNameQuery } from "../services/pokeapi";
import PokeDisplay from "../components/displays/PokeDisplay";
import Header from "../components/Header";


const PokeInfo = function()
{
    // Hooks.
    // const [pokemon, setPokemon] = useState({});
    const { pokemonName } = useParams();
    const { data, error, isLoading } = useGetPokemonByNameQuery(pokemonName);

    const pokeJSX = (error ? <h2>Error Fetching Pokemon</h2>
        : isLoading ? <>Loading...</>
            : <PokeDisplay pokemon={data} />);
    

    return (
            <section className="page">
                <Header text="Pokemon Info" />
                {pokeJSX}
            </section>
        );
};


export default PokeInfo;

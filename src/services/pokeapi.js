import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";


const BASE_URL = "https://pokeapi.co/api/v2";
const pokeApi = createApi({
    reducerPath: "pokeApi",
    baseQuery: fetchBaseQuery({ baseUrl: BASE_URL }),
    endpoints: (builder) => ({
        getAllPokemon: builder.query({
            query: (_) => `pokemon?limit=2000`,
        }),
        getPokemonByName: builder.query({
            query: (name) => `pokemon/${name}`,
        }),
    }),
});


export const { useGetAllPokemonQuery, useGetPokemonByNameQuery } = pokeApi;
export { pokeApi };

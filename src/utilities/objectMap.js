const objectMap = function(obj, fn)
{
    /*Convert object into entries (list of [key, value] pairs) so that
    Array.prototype.map can be used.
    Array.prototype.map is given a function that takes a single
    entry, and an index. This function applies the mapped 'fn'
    functon to each value, and returns the same key with this
    function applied to the value.
    Finally, 'Object.fromEntries' converts the entries back into
    an object.
    */
    return Object.fromEntries(
        Object.entries(obj).map(
            ([key, value], index) =>
            {
                return [key, fn(value, key, index)];
            }
        )
    );
};


export default objectMap;

const capitalize = function(str)
{
    const [firstCharacter, ...restStr] = str;
    return [firstCharacter.toUpperCase(), ...restStr].join("");
};

const capitalCase = function(str, separator = " ")
{
    const stringParts = str.split(separator);
    return stringParts.map((stringPart) => 
    {
        return capitalize(stringPart);
    }).join(separator);
}


export default capitalCase;
export
{
    capitalize,
    capitalCase,
};

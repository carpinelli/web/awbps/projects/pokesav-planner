import { configureStore, getDefaultMiddleware, } from "@reduxjs/toolkit";

// import configReducer from "./configSlice";
import { pokeApi } from "../services/pokeapi";
import pokeReducer from "./pokeSlice";
import { setupListeners } from "@reduxjs/toolkit/dist/query";


const store = configureStore({
    reducer: {
        // configs: configReducer,
        pokes: pokeReducer,
        [pokeApi.reducerPath]: pokeApi.reducer,
    },
    // Adding the API middleware enables caching, invalidation, polling,
    // and other useful features of 'rtk-query'.
    middleware: (getDefaultMiddleware) =>
    {
        return getDefaultMiddleware().concat(pokeApi.middleware);
    },
});
// Store entire state in local storage. To be refined in order to avoid
// saving the entire state on every refresh.
// store.subscribe(() =>
// {
//     localStorage.setItem("pokes", JSON.stringify(store.getState().pokes));
// });
setupListeners(store.dispatch);


export
{
    store,
};

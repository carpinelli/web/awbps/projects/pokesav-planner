import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    pokeLoading: "idle",
    pokes: [],
};
// const loadedState = localStorage.getItem("reduxState");
// const initialState = !loadedState ? blankState : JSON.parse(loadedState);

const pokeSlice = createSlice({
    name: 'pokes',
    initialState,
    reducers: {
        addPoke(state, action)
        {
            const poke = {
                name: action.payload,
                id: null,
            };
            state.list.push(poke);
        },
        removePoke(state, action)
        {
            const index = state.list.findIndex((poke) => poke.name === action.payload);
            state.list.splice(index, 1);
        },
    },
});


export default pokeSlice.reducer;
export const
{
    addPoke,
    removePoke,
} = pokeSlice.actions;

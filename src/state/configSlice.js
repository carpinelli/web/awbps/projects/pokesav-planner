import { useEffect, useState } from "react";


const useLocalStorage = function(key, initialValue)
{
    const [storedValue, setStoredValue] = useState(() =>
    {
        try
        {
            const item = window.localStorage.getItem(key);
            return item ? JSON.parse(item) : initialValue;
        }
        catch (exception)
        {
            console.log(exception);
            return initialValue;
        }
    });

    const setValue = function(value)
    {
        try
        {
            const valueToStore = value instanceof Function ? value(storedValue) : value;
            setStoredValue(valueToStore);
            window.localStorage.setItem(key, JSON.stringify(valueToStore));
        }
        catch (exception)
        {
            console.log(exception);
        }
    }

    return [storedValue, setValue];
};

const useDarkMode = function()
{
    const [enabled, setEnabled] = useLocalStorage("dark-theme");
    const isEnabled = typeof enabledState === "undefined" && enabled;

    useEffect(() =>
    {
        const className = "dark";
        const bodyClass = window.document.body.classList;

        isEnabled ? bodyClass.add(className) : bodyClass.remove(className);
    }, [enabled, isEnabled]);

    return [enabled, setEnabled];
};


export default useDarkMode;

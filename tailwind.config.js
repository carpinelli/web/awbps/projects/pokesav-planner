/** @type {import('tailwindcss').Config} */
export default
{
  content: [
    "./index.html",
    "./src/**/*.{html,js,ts,jsx,tsx}",
  ],
  darkMode: "class",
  theme:
  {
    extend:
    {
      colors:
      {
        primary: "#2a75bb",
        secondary: "#ffcb05",
        subprimary: "#3c5aa6",
        subsecondary: "#c7a008",
      }
    },
  },
  plugins: [],
};


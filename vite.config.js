import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: "/web/awbps/projects/pokesav-planner/",
  server: {
    host: true,
    port: 8000, // This is the port used in Docker.
    watch: {
      usePolling: true,
    },
  },
});
